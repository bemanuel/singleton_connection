package prjInner;

public class MinhaClasse {

	class Conteudo {
		private int i = 11;

		public int value() {
			return i;
		}
	}

	class Destino {
		private String label;

		public Destino(String paraOnde) {
			label = paraOnde;
		}

		String lerLabel() {
			return label;
		}
	}

	public void encomenda(String destino) {
		Conteudo c = new Conteudo();
		Destino d = new Destino(destino);
		System.out.println("To:" + d.lerLabel());
	}
	
	public static void main(String[] args) {
		MinhaClasse mc = new MinhaClasse();
		mc.encomenda("Cracovia");
	}
	
	public Destino getDestino(String dest) {
		return new Destino(dest);
	}
}
