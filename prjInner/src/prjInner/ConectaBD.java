package prjInner;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public class ConectaBD {
	private static Connection con;
	
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		if (con == null) {
			createConnection();
		}
		return con;
	}
	
	private void createConnection() 
			throws ClassNotFoundException, 
						 SQLException {
		Class.forName("org.postgresql.Driver");
		String url = "jdbc:postgresql://172.16.2.245:5432/dell";
		Properties props = new Properties();
		props.setProperty("user", "aluno");
		props.setProperty("password", "aluno");
		con = DriverManager.getConnection(url, props);
	}
}
